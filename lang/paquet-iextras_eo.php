<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-iextras?lang_cible=eo
// ** ne pas modifier le fichier **

return [

	// I
	'iextras_description' => 'Aldonas en la privata spaco kompletan interfacon por mastrumi ekstrajn kampojn en redaktaj objektoj.',
	'iextras_nom' => 'Ekstraj kampoj (interfaco)',
	'iextras_slogan' => 'Disponigas grafikan interfacon por mastrumi ekstrajn kampojn',
	'iextras_titre' => 'Ekstraj kampoj (interfaco)',
];
