<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-iextras?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// I
	'iextras_description' => 'Añadir en el espacio privado una interfaz completa de gestión de campos adicionales dentro de los objetos editoriales.',
	'iextras_nom' => 'Campos Adicionales (Interfaz)',
	'iextras_slogan' => 'Ofrece una interfaz gráfica para administrar campos adicionales',
	'iextras_titre' => 'Campos Adicionales (Interfaz)',
];
