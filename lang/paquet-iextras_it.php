<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-iextras?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// I
	'iextras_description' => 'Aggiunge nell’area riservata un’interfaccia completa per la gestione di campi aggiuntivi negli oggetti editoriali.',
	'iextras_nom' => 'Campi Extra (Interfaccia)',
	'iextras_slogan' => 'Interfaccia per gestire i campi extra',
	'iextras_titre' => 'Campi Extra (Interfaccia)',
];
