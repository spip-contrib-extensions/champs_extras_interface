<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-iextras?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// I
	'iextras_description' => 'Inclui, na área restrita, uma interface completa de gerenciamento de campos suplementares nos objetos editoriais.',
	'iextras_nom' => 'Campos Extras (Interface)',
	'iextras_slogan' => 'Oferece uma interface gráfica para gerenciar os campos extras',
	'iextras_titre' => 'Campos Extras (Interface)',
];
