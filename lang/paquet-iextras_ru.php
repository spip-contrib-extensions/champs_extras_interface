<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-iextras?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// I
	'iextras_description' => 'Полноценный интерфейс для добавлениях новых полей к статьям, разделам и прочим объектам а административной части сайта.',
	'iextras_nom' => 'Создание новых полей - интерфейс (Champs Extras Interface)',
	'iextras_slogan' => 'Интерфейс для добавления новых полей к существующим объектам',
	'iextras_titre' => 'Создание новых полей - интерфейс (Champs Extras Interface)',
];
